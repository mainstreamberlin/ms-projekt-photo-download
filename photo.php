<?php
/*
Plugin Name: ATH Photos download
Plugin URI:
Description:
Version: 1.201702.24
Author: Anh-Tuan Hoang
Author URI:
*/
if(!class_exists('MSDashboard')){
    wp_die( sprintf( __("No plugins found for &#8220;%s&#8221;."), "MSDashboard - ms_main" ) .'<br><br>'. plugin_dir_path( __FILE__ ) );
}
require('classes/adagio.php');
require('classes/g030.php');
require('classes/virtualnight.php');

if( is_admin() )
{
	new MSPhotos;
}
/**
 *
 */
class MSPhotos
{
  public $dashicon = 'dashicons-camera';
  /**
 	 * Block comment
 	 *
 	 * @param type
 	 * @return void
	 */
  function __construct()
  {
    add_action( 'admin_menu', array($this, 'admin_menu' ));
    add_action( 'admin_enqueue_scripts', array($this, 'enqueue') );

    /*ADAGIO*/
		add_action( 'wp_ajax_photo_download_PhotoAdagio_galerylist_contents', array('MSPhotoAdagio', 'ajax_galerylist_contents') );
		add_action( 'wp_ajax_nopriv_photo_download_PhotoAdagio_galerylist_contents', array('MSPhotoAdagio' ,'ajax_galerylist_contents') );

    add_action( 'wp_ajax_photo_download_PhotoAdagio_gallery_title', array('MSPhotoAdagio', 'get_title') );
		add_action( 'wp_ajax_nopriv_photo_download_PhotoAdagio_gallery_title', array('MSPhotoAdagio' ,'get_title') );

    add_action( 'wp_ajax_photo_download_PhotoAdagio_gallery', array('MSPhotoAdagio', 'get_contents') );
		add_action( 'wp_ajax_nopriv_photo_download_PhotoAdagio_gallery', array('MSPhotoAdagio' ,'get_contents') );

    /*G030*/
    add_action( 'wp_ajax_photo_download_PhotoG030_gallery', array('MSPhotoG030', 'g030_main') );
		add_action( 'wp_ajax_nopriv_photo_download_PhotoG030_gallery', array('MSPhotoG030' ,'g030_main') );

    /*Virtualnights*/
    add_action( 'wp_ajax_photo_download_MSPhotoVirtualnights_galerylist_contents', array('MSPhotoVirtualnights', 'ajax_galerylist_contents') );
		add_action( 'wp_ajax_nopriv_photo_download_MSPhotoVirtualnights_galerylist_contents', array('MSPhotoVirtualnights' ,'ajax_galerylist_contents') );

    add_action( 'wp_ajax_photo_download_MSPhotoVirtualnights_galerylist_pages', array('MSPhotoVirtualnights', 'ajax_galerylist_pages') );
    add_action( 'wp_ajax_nopriv_photo_download_MSPhotoVirtualnights_galerylist_pages', array('MSPhotoVirtualnights' ,'ajax_galerylist_pages') );

    add_action( 'wp_ajax_photo_download_MSPhotoVirtualnights_gallery', array('MSPhotoVirtualnights', 'main') );
		add_action( 'wp_ajax_nopriv_photo_download_MSPhotoVirtualnights_gallery', array('MSPhotoVirtualnights' ,'main') );


  }
  /*
		fn
	*/
	public function admin_menu(){
		global $msphoto_admin_page;

		$msphoto_admin_page = add_menu_page('Fotos downloaden', 'Fotos-Download', 'edit_pages', 'ath-photos-download', array($this, 'page_actions'), $this->dashicon, 22);
    $msphoto_admin_page_02 = add_submenu_page( 'ath-photos-download', 'G030', 'G030', 'edit_pages', 'ath-photos-download-g030', array($this, 'page_030_actions'));
    $msphoto_admin_page_03 = add_submenu_page( 'ath-photos-download', 'Virtual Nights', 'Virtual Nights', 'edit_pages', 'ath-photos-download-virtualnights', array($this, 'page_virtualnights_actions'));
  }
  /**
 	 * Block comment
 	 *
 	 * @param type
 	 * @return void
	 */
  public function enqueue() {
 		wp_enqueue_style( 'plugin-MSPhotos', plugin_dir_url(__FILE__) .'css/style.css' );
    wp_enqueue_script( 'ath-MSPhotos-script', plugin_dir_url(__FILE__). 'js/script.js' );
 	}
  /**
 	 * Block comment
 	 *
 	 * @param type
 	 * @return void
	 */
	public function page_actions(){
  ?>
  <div class="wrap">
    <h2 class="page-title"><span class="dashicons <?php echo $this->dashicon ?>"></span> <?php echo $GLOBALS['title'] ?></h2>
    <?php
    $MSPhotoAdagio = new MSPhotoAdagio;
    $MSPhotoAdagio->display();
    ?>
  </div>
  <?php
  }
  /**
 	 * Block comment
 	 *
 	 * @param type
 	 * @return void
	 */
	public function page_030_actions(){
  ?>
    <div class="wrap">
      <h2 class="page-title"><span class="dashicons <?php echo $this->dashicon ?>"></span> <?php echo $GLOBALS['title'] ?></h2>
      <?php
      $MSPhotoG030 = new MSPhotoG030;
      $MSPhotoG030->display();
      ?>
    </div>
  <?php
  }
  /**
 	 * Block comment
 	 *
 	 * @param type
 	 * @return void
	 */
	public function page_virtualnights_actions(){
  ?>
    <div class="wrap">
      <h2 class="page-title"><span class="dashicons <?php echo $this->dashicon ?>"></span> <?php echo $GLOBALS['title'] ?></h2>
      <?php
      $MSPhotoVirtualnights = new MSPhotoVirtualnights;
      $MSPhotoVirtualnights->display();
      ?>
    </div>
  <?php
  }
}

?>
