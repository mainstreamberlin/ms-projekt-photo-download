<?php
/**
 *
 */
class MSPhotoVirtualnights
{
  public $domain = 'https://www.virtualnights.com';
  public $gallerylistUrl = 'https://www.virtualnights.com/berlin/partybilder';

  function __construct()
  {

  }
  /**
 	 * Block comment
 	 *
 	 * @param type
 	 * @return void
	 */

  public function display(){
    $args = array(
    "postbox_class" => array("first" => "col-9 first", "second" => "col-3 last")
    );

    $widgets = array(
      array(
      "align" => "first",
      "id"    => "photoDownload-form",
      "title" => $GLOBALS['title'],
      "data"  => $this->form()),
      array("align" => "first", "id"    => "photoDownload-gallery", "button" => array( array("title" => "Download", "href" => "#", "class" => "button-primary photos_title_show_download") ),"title" => "Gallery", "data" => '<div class="clearfix photos_show"></div>' ),
      array("align" => "second", "title" => $GLOBALS['title'] .'-Galerien', "data" => $this->galerylist() ),
    );


    $MSDashboard = new MSDashboard( $widgets, $args );
    echo $MSDashboard->display();
  }
  public function form(){
    $out .= '<form id="virtualnight-photos-download">';
    $out .= '<input name="page" type="hidden" value="'. $_REQUEST["page"] .'">';
  	$out .= '
  	 <table class="links-table">
          <tr>
            <td><input name="pfad" type="text" class="url" value="'. $_REQUEST["pfad"] .'">
              <br>Pfad: ganze Url von <a href="http://www.virtualnights.com/berlin/partybilder/" target="_blank">'.$GLOBALS['title'].'</a></td>
          </tr>
          <tr>
            <td><input name="anzahl" type="text" class="page_count" value="'. (($_REQUEST['anzahl']) ? $_REQUEST['anzahl'] : 20) .'">
              <br>Anzahl der Seiten</td>
          </tr>
          <tr>
            <td><input name="submit" type="submit" class="button button-primary" value="Bilder anzeigen"></td>
          </tr>
        </table>
  	';
  	$out .= '</form>';
    return $out;
  }
  public function main(){
    $MSPhotoVirtualnights = new MSPhotoVirtualnights;
    $web = $_REQUEST["pfad"];
    $anzahl = $_REQUEST["anzahl"];
    $p = 0;
    $page = ($_REQUEST["page"]) ? ($_REQUEST["page"]-1) : "0";
    $web = $web;

    $inhalt = file_get_contents($web);
    preg_match_all("#<a itemprop=\"url\" href=\"(.+?)\" #", $inhalt, $urls);

    for($i=0; $i < count($urls[0]); $i++){
    	$p = $p+1;

    	$url = str_replace(array('<a itemprop=\"url\" href=\"', '"'), array(""), $urls[0][$i]);

    	$url = explode(" ",trim($url) );
    	$image_page = str_replace( array("href="), "", $url[2]);
    	$p_nr = ($page * 19)+$p;
      $image = $MSPhotoVirtualnights->vn_single_main( $MSPhotoVirtualnights->domain . $image_page );
    	echo '<div class="bild"><a href="'.$image.'" download><img src="' . $image .'" height="50" class="img-responsive"></a><p>'.$p_nr.'</p></div>';
    }
    wp_die();
  }
  public function vn_single_main( $image_page ){

  	$web = $image_page;
  	$inhalt = file_get_contents($web);
  	preg_match_all("#<img class=\"main-pic\" src=\"(.+?)\" #", $inhalt, $city);

  	$stadt = str_replace(array('<img class=\"main-pic\" src=\"', '"'), array(""), $city[0][0]);
  	$stadt = explode(" ",trim($stadt) );

  	preg_match_all("#<span>(.+?)</span>#", $inhalt, $aufbau);
  	$detail = $aufbau[0];
  	$detail = str_replace(array('<span>', '</span>'), array(""), $detail);
  	preg_match_all("#<img src=(.+?)>#", $inhalt, $img);



  	return str_replace( array("src="), "", $stadt[2]);

  }
  /**
 	 * Block comment
 	 *
 	 * @param type
 	 * @return void
	 */

  public function galerylist(){
    $out = '<div class="galerylist loading" data-action="photo_download_MSPhotoVirtualnights_galerylist_contents" data-web="https://www.virtualnights.com">';
    $out .= '</div>';
    return $out;
  }
  /**
 	 * Block comment
 	 *
 	 * @param type
 	 * @return void
	 */

  public function ajax_galerylist_contents()
  {
    /*var_dump($web);*/
    $MSPhotoVirtualnights = new MSPhotoVirtualnights;
    $web 		       = $MSPhotoVirtualnights->gallerylistUrl;

		$inhalt        = file_get_contents($web);
    $inhalt = str_replace("data-src", "src", $inhalt);

		preg_match_all('/<ul class=\"listing gallery\" id=\"partypiclisting\">(.*?)<\/ul>/s',$inhalt,$content);


    wp_send_json( $content );
    wp_die();
  }
  public function ajax_galerylist_pages()
  {
    /*var_dump($web);*/
    $MSPhotoVirtualnights = new MSPhotoVirtualnights;
    $web 		       = $MSPhotoVirtualnights->gallerylistUrl;

		$inhalt        = file_get_contents($web);
		preg_match_all('/<ol class=\"pagination up\">(.*?)<\/ol>/s',$inhalt,$content);

    wp_send_json( $content );
    wp_die();
  }
}
?>
