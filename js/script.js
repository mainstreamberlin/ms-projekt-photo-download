jQuery(document).ready(function ($) {
  $(".galerylist").html( msloadingImage );
	setTimeout( function(){

		$.ajax({
			   type: "POST",
			   url: ajaxurl,
			   data: "action=" + $(".galerylist").attr("data-action"),
			   dataType: 'json',
			   success: function( data ){
					var out	= '';
					//console.log(1);
					$.each( data, function( i, value ){
						out += '<p>'+value+'</p>';
					});
					$(".galerylist").html( out );
			   }
		});

	}, 100);

	$("body").on( "click", ".galerylist a", function(){
		var url = $(".galerylist").data("web") + $.trim( $(this).attr("href") );
		//console.log( url );
		$(".url").val( url );
		$(".show_all_pics, #virtualnight-photos-download .button").click();
		return false;
	});

  $("body").on("click", "#photoDownload-form .show_all_pics", function(){

		$(".photos_show").html( msloadingImage );
		$.ajax({
			   type: "POST",
			   url: ajaxurl,
			   data: "action=photo_download_PhotoAdagio_gallery_title&pfad=" + $("#photoDownload-form .url").val(),
			   dataType: 'json',
			   success: function( data ){
				       $(".photos_title_show, title").html( data + '<button class="alignright button button-primary photos_title_show_download">Download</button>');
			   }
		});

		$.ajax({
			   type: "POST",
			   url: ajaxurl,
			   data: "action=photo_download_PhotoAdagio_gallery&pfad=" + $("#photoDownload-form .url").val(),
			   dataType: 'json',
			   success: function( data ){
					var out	= '';
					$(".photos_title_show_count").text( 'Anzahl: ' + data.length );
					$("title").text( '_Bilder_' + $(".photos_title_show").text() + '_anz' + data.length);

          $.each( data, function( i, value ){
						out += '<div class="bild"><a href="'+value+'" download><img src="'+value.replace("large", "thumbnail")+'" height="50"></a>'+msloadingImage+'</div>';
					});
					$(".photos_show").html( out );
			   }
		});

		return false;
	});
  $("body").on("click", ".photos_title_show_download", function(){
		$(".photos_show .bild").each( function (){
			$(this).find("a")[0].click();
			//console.log( $(".photos_show .bild:first-child a").attr("href") );
		});
    return false;
	});

  /*
    G030
  */
  $("#g030-photos-download").submit( function(){
		var vn_seite_anzahl = $(this).find(".page_count").val();
		var vn_url = $(this).find(".vn_url").val();
		var out = '<div class="seite">';

		for( var i = 1; i <= vn_seite_anzahl; i++ ){
			out = out + '<div class="bild" id="bild-' + i + '">' + i + msloadingImage + '</div>';
		}
		out = out + '</div>';
		$(".photos_show").html( out );

		for( var i = 1; i <= vn_seite_anzahl; i++ ){

			$(".photos_show #bild-" + i).load( ajaxurl + "?action=photo_download_PhotoG030_gallery&bild="+i+"&pfad=" + vn_url + "/bild-" + i);
		}

		return false;
	});
  /*
    virtualnight
  */
  $("#virtualnight-photos-download").submit( function(){
		var vn_seite_anzahl = $(this).find(".page_count").val();
		var vn_url = $(this).find(".url").val();
		var out = "";
		for( var s=1; s <= vn_seite_anzahl; s++){
			out = out + '<div class="seite" data-page="' + s + '">Seite ' + s + '<div class="sub clearfix" id="seite-' + s + '">' + msloadingImage +'</div></div>';
		}
		$(".photos_show").html( out );

		$(".photos_show .seite").each(function( i ){
			//alert ( $(this).text() );
			$(this).find(".sub").load( ajaxurl + "?action=photo_download_MSPhotoVirtualnights_gallery&page=" + $(this).data("page") +"&pfad=" + vn_url + "?page=" + $(this).data("page"),
      function(){
        setTimeout( function(){
          $(".photos_show .sub .bild").each(function( i ){
            $(this).find("p").text(i);
          });
        },i*100);
      });
    });
		return false;
	});
});
