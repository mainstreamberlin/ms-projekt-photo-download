<?php
/**
 *
 */
class MSPhotoAdagio
{
  public $gallerylistUrl = 'http://www.adagio.de/adagio/gallerien';
  public $domain 	= "http://www.adagio.de/";

  function __construct()
  {
    if( !$this->gallerylistUrl ) return 'keine Gallery';
  }
  public function display(){
    $args = array(
    "postbox_class" => array("first" => "col-9 first club-adagio", "second" => "col-3 last")
    );

    $widgets = array(
      array(
      "align" => "first",
      "id"    => "photoDownload-form",
      "title" => "ADAGIO",
      "data"  => $this->form()),
      array("align" => "first", "id"    => "photoDownload-gallery", "title" => "Gallery", "data" => $this->gallery() ),
      array("align" => "second", "title" => "ADAGIO Events", "data" => $this->galerylist() ),
    );

    $MSDashboard = new MSDashboard( $widgets, $args );
    echo $MSDashboard->display();
  }
  /**
 	 * Block comment
 	 *
 	 * @param type
 	 * @return void
	 */

  public function form(){
    $out = '<table class="wp-list-table widefat striped">';
    $out .= '<tr>
            <td>Url</td>
            <td><input name="pfad" type="text" class="regular-text url" value="">
				        <p>Pfad: http://adagio.de/adagio/galleries/view/91</p></td></tr>';
    $out .= '<tr>
            <td></td>
            <td><input name="submit" type="submit" value="Bilder anzeigen" class="button button-primary show_all_pics"></td>
            </tr>';
    $out .= '</table>';
    return $out;
  }
  /**
 	 * Block comment
 	 *
 	 * @param type
 	 * @return void
	 */

  public function galerylist(){
    $out = '<div class="galerylist loading" data-action="photo_download_PhotoAdagio_galerylist_contents" data-web="http://adagio.de">';
    $out .= '</div>';
    return $out;
  }
  /**
 	 * Block comment
 	 *
 	 * @param type
 	 * @return void
	 */

  public static function ajax_galerylist_contents()
  {
    $MSPhotoAdagio = new MSPhotoAdagio;
    $web 		       = $MSPhotoAdagio->gallerylistUrl;
		$inhalt        = file_get_contents($web);
		preg_match_all('/<div id=\"content-header-featured\">(.*?)<\/div>/s',$inhalt,$content3);
		preg_match_all('/<div class=\"gallery-list-item-footer\">(.*?)<\/div>/s',$inhalt,$content);
		preg_match_all('/<div class=\"gallery-block-info\">(.*?)<\/div>/s',$inhalt,$content2);
		$newcontent = array_merge($content3[0], $content[0], $content2[0]);
    wp_send_json( $newcontent  );
  }
  /**
 	 * Block comment
 	 *
 	 * @param type
 	 * @return void
	 */

  public function gallery(){
    $out = '<div class="photos_title_show"></div>';
    $out .= '<div class="photos_title_show_count"></div>';
    $out .= '<div class="photos_show clearfix"></div>';
    return $out;
  }
  public static function get_title(){
    $MSPhotoAdagio = new MSPhotoAdagio;

		$web 		   = isset($_REQUEST["pfad"]) ? $_REQUEST["pfad"] : '';
    if(!$web){
      wp_die();
    }
		$inhalt 	 = file_get_contents($web);
		preg_match_all ("/<div id=\"content-header-text\">([^`]*?)<\/div>/", $inhalt, $content);

		$replace	= array('<div id="content-header-text">', '</div>', '<h1>', '</h1>', '<span class="event-date">', '</span>');
		$content[0]	= str_replace($replace, array(""), $content[0]);
		//print_r( $content );
		/*return $content[0];*/
    wp_send_json( $content[0]  );
	}
  /**
 	 * Block comment
 	 *
 	 * @param type
 	 * @return void
	 */

	public static function get_contents(){
    $MSPhotoAdagio = new MSPhotoAdagio;

    $web 		   = isset($_REQUEST["pfad"]) ? $_REQUEST["pfad"] : '';
    if(!$web){
      wp_die();
    }

		$inhalt = file_get_contents($web);
		preg_match_all("#href=\"(.+?)\"#", $inhalt,  $urls);
		foreach( $urls[0] as $url ){
			$exists_jpg = strpos(strtolower( $url ), "jpg");

			if ( $exists_jpg > 0){
				$url 			= str_replace(array('href="/', '"'), array(""), $url);
				$exists_http	= strpos(strtolower( $url ), "http://www.");
				if ( $exists_http < 1 ){
				$url			= $MSPhotoAdagio->domain . $url;
				}
				$newurls[]		= $url;
				//echo $url . '<br>';
			}
		}

		//print_r( $newurls );
		wp_send_json( $newurls );
	}
}

?>
